from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class GroupChat(models.Model):
	nama_group = models.CharField("Nama Group", max_length=50, blank=True, null=True)
	anggota = models.ManyToManyField(User)

	def __unicode__(self):
		if self.nama_group:
			return self.nama_group
		else:	
			return "-"

	class Meta:
		verbose_name="Group Chat"
		verbose_name_plural="Group Chat"

class Pesan(models.Model):
	pesan = models.CharField("Pesan", max_length=255)
	dibuat_pada = models.DateTimeField(auto_now_add=True)
	pengirim = models.ForeignKey(User, verbose_name="Pengirim")
	group_chat = models.ForeignKey(GroupChat, verbose_name="Group Chat")

	def __unicode__(self):
		return self.pesan

	def save(self, *args, **kwargs):
		''' On save, update timestamps '''
		if not self.id:
			self.created_at = datetime.now()
		return super(Pesan, self).save(*args, **kwargs)

	class Meta:
		verbose_name="Pesan"
		verbose_name_plural="Pesan"

		
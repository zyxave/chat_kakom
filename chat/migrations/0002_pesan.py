# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chat', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pesan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pesan', models.CharField(max_length=255, verbose_name=b'Pesan')),
                ('group_chat', models.ForeignKey(verbose_name=b'Group Chat', to='chat.GroupChat')),
                ('pengirim', models.ForeignKey(verbose_name=b'Pengirim', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Pesan',
                'verbose_name_plural': 'Pesan',
            },
        ),
    ]

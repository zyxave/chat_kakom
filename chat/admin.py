from django.contrib import admin
from chat.models import GroupChat, Pesan

# Register your models here.

admin.site.register(Pesan)
admin.site.register(GroupChat)
